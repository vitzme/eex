-module(effects).
-export([print/1,even_print/1]).

print(N) -> print(N, 0).
print(N, X) when N > 1 ->
    io:format("~p ", [X + 1]),
    print(N - 1, X + 1);
print(1, X) ->
    io:format("~p~n", [X + 1]).

even_print(N) when N >= 2 -> even_print(N, 0).
even_print(2, X) ->
    io:format("~p~n", [X + 2]);
even_print(N, X) when N rem 2 == 0 ->
    io:format("~p ", [X + 2]),
    even_print(N - 2, X + 2);
even_print(N, X) when N rem 2 =/= 0 ->
    even_print(N - 1, X).


%nprint(N) when N == 1 ->
%    io:format("~w~n", [1]);
%nprint(N) when N > 1 ->
%    io:format("~w~n", [N]),
%    nprint(N-1).


%neven_print(N) when N == 2 -> io:format("~w~n", [2]);
%neven_print(N) when N rem 2 == 1 -> neven_print(N-1);
%neven_print(N) when N rem 2 == 0 ->
%    io:format("~w~n", [N]),
%    neven_print(N - 1).
