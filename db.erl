-module(db).
-export([new/0,write/3,delete/2,read/2,match/2,destroy/1]).

new() ->
    [].

write(Key, Element, DbRef) -> write(Key, Element, DbRef, DbRef).
write(Key, Element, DbRef, []) ->
    [{Key, Element}] ++ DbRef;
write(Key, Element, DbRef, CurrentDbRef) ->
    [Head|Tail] = CurrentDbRef,
    if {Key, Element} == Head ->
        DbRef;
        true -> write(Key, Element, DbRef, Tail)
    end.

delete(Key, DbRef) -> delete(Key, DbRef, []).
delete(_, [], Acc) ->
    Acc;
delete(Key, DbRef, Acc) ->
    [Head|Tail] = DbRef,
    {K, _} = Head,
    if K == Key ->
        Acc ++ Tail;
        true -> delete(Key, Tail, Acc ++ [Head])
    end.


read(_, []) ->
    {error, instance};
read(Key, DbRef) ->
    [Head|Tail] = DbRef,
    {CurrentKey, Element} = Head,
    if Key == CurrentKey ->
        {ok, Element};
        true -> read(Key, Tail)
    end.


match(Element, DbRef) -> match(Element, DbRef, []).
match(_, [], Result) ->
    Result;
match(Element, DbRef, Result) ->
    [Head|Tail] = DbRef,
    {Key, CurrentElement} = Head,
    if CurrentElement == Element ->
        match(Element, Tail, [Key] ++ Result);
        true -> match(Element, Tail, Result)
    end.


destroy(_DbRef) ->
    ok.
