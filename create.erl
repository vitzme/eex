-module(create).
-export([create/1,reverse_create/1]).

create(N) -> create(N, []).
create(N, X) when N > 0 ->
    create(N - 1, [N] ++ X);
create(0, X) ->
    X.

reverse_create(N) -> reverse_create(N, []).
reverse_create(N, X) when N > 0 ->
    reverse_create(N - 1, [N|X]);
reverse_create(0, X) ->
    X. 
