-module(eex).
-export([sum/1,printAll/1,reverse_create/1,create/1]).

sum([]) ->
    0;
sum([H|T]) ->
    H + sum(T).

printAll([]) ->
    io:format("~n", []);
printAll([H|T]) ->
    io:format("~p", [H]),
    printAll(T).

%reverse_create(N) when N > 0 ->
%    [N|reverse_create(N-1)];
%reverse_create(0) ->
%    [].

%create(N) when N > 0 ->
%    create(N-1) ++ [N];
%create(0) ->
%    [].

create(N) -> create(N, []).
create(N, X) when N > 0 ->
    create(N - 1, [N] ++ X);
create(0, X) ->
    X.

reverse_create(N) -> reverse_create(N, []).
reverse_create(N, X) when N > 0 ->
    reverse_create(N - 1, [N|X]);
reverse_create(0, X) ->
    X.

%% old create/reverse_create
%create(N) -> create(N, [N]).
%create(1, C) -> C;
%create(N, C) when N > 1 -> create(N-1, [N-1] ++ C).
     
%reverse_create(N) -> reverse_create(N, [N]).
%reverse_create(1, C) -> C;
%reverse_create(N, C) when N > 1 -> reverse_create(N-1, C ++ [(N-1)]).
